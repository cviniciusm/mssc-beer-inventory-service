DROP DATABASE IF EXISTS beersinventoryervice;

DROP USER IF EXISTS `beer_inventory_service`@`%`;

CREATE DATABASE IF NOT EXISTS beerinventoryservice CHARACTER SET utf8;

CREATE USER IF NOT EXISTS `beer_inventory_service`@`%` IDENTIFIED WITH mysql_native_password BY 'password';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, EXECUTE, CREATE VIEW, SHOW VIEW,
    CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER ON `beerinventoryservice`.* TO `beer_inventory_service`@`%`;
    
FLUSH PRIVILEGES;